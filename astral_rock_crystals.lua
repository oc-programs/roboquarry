local args = {...}
local need_crystals = tonumber(args[1]) or 3
local component = require("component")
local r = component.robot
local ic = component.inventory_controller
local geo = component.geolyzer

local function count_as_crystal(slot)
	local stack = r.count(slot)
	if stack ~= 0 then
	local name = ic.getStackInInternalSlot(slot).name
		if string.find(name, "astralsorcery") and string.find(name, "crystal") then
			return 1
		end
	end
	return 0
end

local function count_crystals()
	local count = 0
	for i = 1, r.inventorySize() do
		count = count + count_as_crystal(i)
	end
	return count
end

local function starlight_consumed()
	local blk_info = geo.analyze(0)
	return blk_info.name == "minecraft:air"
end

local function fill_pool()
	r.drain(3)
	r.fill(0)
end

while count_crystals() < need_crystals do
	fill_pool()
	r.select(1)
	r.drop(0)
	while not starlight_consumed() do
		os.sleep(5)
	end
	r.suck(0)
	r.suck(0)
end
