r = component.proxy(component.list("robot")())
pc = computer
uptime = 0
time_quant = 0.5
mixture_slot = 2
mixing_time = 8

for i = 1, r.inventorySize() do
  if r.count(i) == 0 then mixture_slot = i - 1  break end
end

function wait(time)
  uptime = pc.uptime() + time
  while uptime < pc.uptime() do
    pc.pullSignal(time_quant)
  end
end

function get_water()
  function mv(vmb, vma)
    r.turn(true)
    r.turn(true)
    if vmb then r.move(vmb) end
    r.move(3)
    r.move(3)
    if vma then r.move(vma) end
    r.use(0)
  end
  for _ = 1, 3 do
    mv(nil, 0)
    mv(1, nil)
  end
end

function mix()
  r.move (1)
  for i = 1, mixture_slot - 1 do
    r.select(i)
    r.drop(0,1)
    wait(1)
  end
  brews = r.count(mixture_slot)
  
  while brews == r.count(mixture_slot) do
    r.suck(0)
    wait(time_quant)
  end
  r.move(0)
  r.select(mixture_slot)
  r.drop(3,1)
end

while r.count(1) > 0 do
  get_water()
  mix()
end

for i = 1, r.inventorySize() do
  r.select(i)
  r.drop(3)
end

r.select(1)
