local sh = require("shell")
local component = require("component")
local side = require("sides")
local serial = require("serialization")
local heightMapFileName = "movingMap"
local bedrock = "minecraft:bedrock"

local args = sh.parse(...)
local line_length = tonumber(args[1])
local lines_count = tonumber(args[2])

local r = component.robot
local ic = component.inventory_controller
local geo = component.geolyzer
component.chunkloader.setActive(true);

local toolsTableStart = 12
local toolsCount = math.min(16, r.inventorySize() - toolsTableStart)
local toolsTable = {}
local charge_time = 10

local function rforward() r.move(side.forward) end
local function rdown() r.move(side.down) end
local function rup() r.move(side.up) end

-- filling heightMap
if args[1] and args[2] and line_length and lines_count then

    local function getDepth(maxDiveDepth)
        local result = -1
        local obstacle, detected = r.detect(side.down)
        if(obstacle or detected == "liquid") then return result end

        local depth = 0
        for j = 1, maxDiveDepth + 1 do
            rdown()
            depth = j
            local block_info = geo.analyze(side.down)
            if block_info.name == bedrock then
                result = j
                break
            end
            local obstacle, detected = r.detect(side.down)
            if(obstacle or detected == "liquid") then break end
        end

        for j = 1, depth do
            rup()
        end

        return result
    end

    local function scan_half_line(run_length, maxDiveDepth, lineRecord)

        for i = 1, run_length do

            local result = getDepth(maxDiveDepth)

            table.insert(lineRecord, result)

            if i < run_length then
                rforward()
            end
        end
    end

    rforward()

    local heightMap = {};
    for lineIndex = 1, lines_count do
        local half_line = {}
        table.insert(heightMap, half_line)
        scan_half_line(line_length, 2, half_line)
        r.turn(true)
        rforward()
        r.turn(true)
        half_line = {}
        table.insert(heightMap, half_line)
        scan_half_line(line_length, 2, half_line)
        if lineIndex < lines_count then
            r.turn(false)
            rforward()
            r.turn(false)
        end
    end


    file=io.open(heightMapFileName .. ".lua","w")
    file:write("return {")
    for i, v in pairs(heightMap) do
        file:write(serial.serialize(v))
        if i < #heightMap then
            file:write(",")
        end
    end
    file:write("}")
    file:close()

    rforward()
    r.turn(true)
    for i = 1, (lines_count * 2) - 1 do
        rforward()
    end
    r.turn(true)

    os.exit(0)
end


local heightMap = require(heightMapFileName)


local function diveUse(returning, diveDepth)
    if not returning  and diveDepth >= 0 then
        for j = 1, diveDepth do
            rdown()
        end

        if r.detect(side.down) then
            component.computer.beep()
        end
        r.use(side.down)

        for j = 1, diveDepth do
            rup()
        end
    end
end

local function run_half_line(lineRecord, returning)
    for i, diveDepth in ipairs(lineRecord) do
        diveUse(returning, diveDepth - 1)
        if i < #lineRecord then
            rforward()
        end
    end
end


local function craftTools()
    r.turn(true)
    for i = 1, r.inventorySize() do
        r.select(i)
        r.drop(side.forward)
    end
    r.turn(false)

    local ironSlot = 4
    local ironSide = side.forward
    r.select(ironSlot) -- iron ingots
    r.suck(ironSide, toolsCount)
    local ironCount = r.count()

    local flintsSlot = 8
    local flintsSide = side.down
    r.select(flintsSlot) -- flints
    r.suck(flintsSide, toolsCount)
    local flintsCount = r.count()

    local craftCycles = math.min(ironCount, flintsCount)
    if craftCycles == 0 then
        print("no resources for crafting")
        os.exit(-2)
    end
    for i = 1, craftCycles do
        r.select(ironSlot)
        r.transferTo(6, 1)
        r.select(flintsSlot)
        r.transferTo(1, 1)
        r.select(1)
        component.crafting.craft()
        local toolsTablePosition = #toolsTable
        r.transferTo(toolsTableStart + toolsTablePosition, 1)
        table.insert(toolsTable, toolsTableStart + toolsTablePosition)
    end

    r.select(1)
    return toolsTable
end

local function getTool()
    if #toolsTable == 0 and haveNoTolls then
        print("robot have no tools")
        os.exit(-3)
    end
    local toolPos = table.remove(toolsTable)
    if not toolPos then return true end
    r.select(toolPos)
    ic.equip()
    r.select(1)
end

local function initTools()
    toolsTable = {}
    for i = 0, toolsCount - 1 do
        local currentSlot = toolsTableStart + i
        r.select(currentSlot)
        if r.count(currentSlot) > 0 then
            table.insert(toolsTable, currentSlot)
        end
    end
    r.select(1)
    return toolsTable
end

local function haveNoTolls()
    ic.equip()
    local count = r.count()
    ic.equip()
    return (count == 0)
end

toolsTable = initTools()
getTool()

while true do
---@diagnostic disable-next-line: undefined-field
    os.sleep(charge_time)
    rforward()
    local noTools = false

    for run_index, heights in ipairs(heightMap) do
        local oddRun = ((run_index % 2) == 1)
        run_half_line(heights, noTools)
        noTools = haveNoTolls()
        if noTools then
            noTools = getTool()
        end

        if run_index < #heightMap then
            r.turn(oddRun)
            rforward()
            r.turn(oddRun)
        end
    end

    -- return to start
    rforward()
    r.turn(true)

    for i = 1, #heightMap - 1 do
        rforward()
    end

    if noTools then
        craftTools()

        getTool()
    end

    r.turn(true)

end