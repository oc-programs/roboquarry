r = component.proxy(component.list("robot")())
pc = computer

function placebreak()
  r.place(3)
  r.swing(3)
  r.suck(3)
end

for i = 1, r.inventorySize() do
  r.select(i)
  blocks = r.count(i)
  for j = 1, blocks do
    placebreak()
  end
end

r.select(1)
pc.beep()