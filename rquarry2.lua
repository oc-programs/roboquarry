r = component.proxy(component.list("robot")())
pc = computer

compare_slots = r.count(r.inventorySize())
fuzzycompare = true
forward = r.count(1)
aside = r.count(2)
depth = r.count(3)
turnright = true and (r.count(4) ~= 0)
floors_layers = (r.count(5) ~= 0) and r.count(5)
dig_room = (compare_slots == 0)
clear_tunnel = (r.count(6) == 0)
charge_time = 7
if r.count(7)~=0 or r.count(8)~=0 then
	charge_time = r.count(9)*60+r.count(8)
end
clnorm = 0xFF0000
clcharge = 0x00FF00

uptime = 0

function start_charge(cl)
	uptime = pc.uptime()
	r.setLightColor(cl or clcharge)
end

function end_charge()
	while (pc.uptime() < uptime + charge_time) or r.detect(1) do
		pc.pullSignal(1)
	end
	pc.beep()
	r.setLightColor(clnorm)
end

if not(forward>0 and aside>0) then
	charge_time=2
	for _=1,5 do
		pc.beep()
	end
	charge_time = 3
	start_charge(0x000000)
	end_charge()
	pc.shutdown()
end

function dig(side, suck)
	r.swing(side)
	if suck then
		r.suck(side)
	end
end

function mv(side, pickup)
	while not r.move(side) do
		dig(side, pickup)
		pc.beep()
	end
end

function check_dig(side, move)
	diggable = r.detect(side)
	if diggable then
		if (dig_room and clear_tunnel) or (move and clear_tunnel) then
			dig(side, true)
		else
			junk = false
			for i = 1, compare_slots do
				r.select(i)
				junk = junk or r.compare(side, fuzzycompare)
			end
			if not junk then
				dig(side, true)
			elseif move or dig_room then
				dig(side, clear_tunnel)
			end
		end
	end
	if move then
		mv(side, clear_tunnel)
	end
end

go = {
	tr = function()
		r.turn(turnright)
	end,
	go = function(dist, dir)
		dir = dir or 3
		for _= 1, dist do
			check_dig(dir, true)
		end
	end,
	gogo = function (dist, seq)
		seq = seq or {{3, true}, {1, false}, {0, false}}
		for _ = 1, dist do
			for _, v in ipairs(seq) do
				check_dig(v[1], v[2])
			end
		end
	end,
	nextline = function(turn)
		r.turn(turnright)
		go.gogo(1)
		r.turn(turnright)
		turnright = turn
	end,
	deeper = function(d)
		d = d or 3
		go.go(d, 0)
		check_dig(0, false)
	end,
	turnaround = function()
		r.turn(false)
		r.turn(false)
	end,
	return_and_drop = function(layer)
		if layer % 2 == 1 then
			go.tr()
			go.go(aside - 1)
			if aside % 2 == 1 then
				go.tr()
				go.go(forward)
			else
				r.turn(not turnright)
			end
		end
		go.go(layer * 3 - 1, 1)
		for slot = compare_slots + 1, r.inventorySize() do
			r.select(slot)
			r.drop(3)
			pc.beep()
		end
	end,
	return_to_dig = function(layer)
		go.go(layer * 3 - 1, 0)
		if layer % 2 == 1 then
			go.tr()
			go.go(aside - 1)
			if aside % 2 == 1 then
				go.tr()
				go.go(forward)
			else
				r.turn(not turnright)
			end
		end
	end,
	dig_layer = function()
		for _ = 1, aside - 1 do
			go.gogo(forward)
			go.nextline(not turnright)
		end
		go.gogo(forward)
		check_dig(0)
	end
}

lastlayer = 0
go.deeper(2)
for layer = 1, depth do
	if floors_layers then
		dig_room = not (layer % floors_layers == 0)
	end
	go.dig_layer()
	lastlayer = layer
	if layer % 2 == 0 then
		go.return_and_drop(layer)
		start_charge()
		end_charge()
		go.return_to_dig(layer)
	end
	if layer ~= depth then
		go.turnaround()
		go.deeper()
	end
end

turnright = not turnright
go.return_and_drop(lastlayer)
pc.shutdown()