local component = require("component")
local s = require("sides")
local r = component.robot
local tc = component.tank_controller
--[[
  robot: at least one fluid tank and the fluid controller

  start from block before first to last lightwell facing collecting reservoir
  give robot some aquamarine or something else to replenish wells

]]
local lightwells = 5
local lightwell_count = 5
local transfer_rate_mb = 50 --immersive engineering pipe
local liquid_level_to_leave = 1
local sleep_time = 30

local levels = {}
for i = 1, lightwells do
  levels[i] = liquid_level_to_leave
end

local function drain_from_well()
  local lightwell_level = tc.getTankLevel(s.up)
  if lightwell_level > liquid_level_to_leave then
    r.drain(s.up, lightwell_level)
  else
    r.use(s.up)
  end
end

local function drain_from_wells( wells_number )
  for i = 1, wells_number do
    r.move(s.forward)
    drain_from_well()
  end
end

local function flush_return( wells_number )
  local tl = r.tankLevel()
  if tl > 0 then
    local fill_cycles = math.modf(tl / transfer_rate_mb) +1
    for i = 1, fill_cycles do
      r.fill(3,transfer_rate_mb)
    end
  end
  r.turn(true)
  r.turn(true)
  for i =1, wells_number do
    r.move(s.forward)
  end
  r.turn(true)
  r.turn(true)
end


while true do
  drain_from_wells(lightwells)
  flush_return(lightwells)
  os.sleep(sleep_time)
end