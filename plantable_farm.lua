ic = component.proxy(component.list("inventory_controller")())
r = component.proxy(component.list("robot")())
pc = computer

forward = r.count(1)
aside = r.count(2)
depth = r.count(3) + 1
initialturn = true and (r.count(4) ~= 0)
turnright = initialturn
line_width = r.count(5) + 1
layer_thickness = r.count(6) + 1
charge_time = 2220
if r.count(7)~=0 or r.count(8)~=0 then
	charge_time = r.count(8)*60+r.count(7)
end

uptime = 0
lefttoplant = 64

clnorm = 0xFF0000
clcharge = 0x00FF00

function start_charge(cl)
	uptime = pc.uptime()
	r.setLightColor(cl or clcharge)
end

function end_charge(cl)
	while (pc.uptime() < uptime + charge_time) or r.detect(1) do
		pc.pullSignal(1)
	end
	pc.beep()
	r.setLightColor(cl or clnorm)
end

if not(forward>0 and aside>0) then
	charge_time=2
	for _=1,4 do
		pc.beep()
	end
	charge_time = 3
	start_charge(0x000000)
	end_charge()
	pc.shutdown()
end

function dig(side, suck)
	r.swing(side)
	if suck then
		r.suck(side)
	end
end

function mv(side, pickup)
	while not r.move(side) do
		dig(side, pickup)
		pc.beep()
	end
	r.suck(side)
end

function check_dig(side, move)
	diggable = r.detect(side)
	if diggable then
		dig(side, true)
	end
	if move then
		mv(side, true)
		r.suck(3)
		r.suck(0)
	end
end

function plant()
	if lefttoplant < 3 then
		r.equip()
	end
	if r.use(0) then
		lefttoplant = lefttoplant - 1
	end
end

function get_seeds()
	r.select(9)
	r.suck(3)
	ic.equip()
	r.select(1)
	r.suck(3)
	lefttoplant=64
end

go = {
	tr = function()
		r.turn(turnright)
	end,
	go = function(dist, dir)
		dir = dir or 3
		for _= 1, dist do
			check_dig(dir, true)
		end
	end,
	gogo = function (dist, seq)
		seq = seq or {{3, true},{0, false}}
		for _ = 1, dist do
			for _, v in ipairs(seq) do
				check_dig(v[1], v[2])
			end
			plant()
		end
	end,
	nextline = function(t)
		r.turn(turnright)
		go.gogo(line_width)
		r.turn(turnright)
		turnright = t
	end,
	deeper = function(d)
		d = d or layer_thickness
		go.go(d, 0)
		check_dig(0, false)
		plant()
	end,
	turnaround = function()
		r.turn(false)
		r.turn(false)
	end,
	tostart = function()
		go.tr()
		go.gogo((aside - 1)*line_width)
		if aside % 2 == 1 then
			go.tr()
			go.go(forward)
		else
			r.turn(not turnright)
		end
	end,
	dig_layer = function()
		for _ = 1, aside - 1 do
			go.gogo(forward)
			go.nextline(not turnright)
		end
		go.gogo(forward)
		turnright = not turnright
	end
}

while true do
	for layer = 1, depth do
		r.select(1)
		go.dig_layer()
		go.tostart()
		if layer < depth then
			go.deeper()
			go.turnaround()
		else
			go.go((depth-1)*layer_thickness,1)
			for slot = 1, r.inventorySize() do
				r.select(slot)
				r.drop(3)
				pc.beep()
			end
			ic.equip()
			r.drop(3)
		end
		turnright = initialturn
	end
	get_seeds()
	go.turnaround()
	start_charge()
	end_charge()
end